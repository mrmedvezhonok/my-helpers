# add custom aliases to ~/.bashrc

fn="~/bashrc"

echo -e "\n\n# ----____----____----____----____----____----" >> ${fn}
echo -e "\n# ____________________________________________" >> ${fn}
echo -e "\n# My aliases" >> ${fn}
echo -e "\n# --------------------------------------------" >> ${fn}
echo -e "\n# ----____----____----____----____----____----" >> ${fn}
echo -e "\n" >> ${fn}
echo -e "\nalias aptlu="apt list --upgradable"" >> ${fn}
echo -e "\nalias aplu="aptlu"" >> ${fn}
echo -e "\nalias aptate="sudo apt update"" >> ${fn}
echo -e "\nalias aptgrade="sudo apt upgrade"" >> ${fn}
echo -e "\n" >> ${fn}
echo -e "\nalias psmem='ps axu --sort=-pmem'" >> ${fn}
echo -e "\nalias pscpu='ps axu --sort=-pcpu'" >> ${fn}
echo -e "\n" >> ${fn}
echo -e "\nalias clip='xclip -selection clipboard'" >> ${fn}
echo -e "\nalias clipo='xclip -selection clipboard -out'" >> ${fn}
echo -e "\n" >> ${fn}
echo -e "\nalias cpwd='echo $(pwd)|clip'" >> ${fn}
echo -e "\n" >> ${fn}
